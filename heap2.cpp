#include<iostream>
#include<stdlib.h>
using namespace std;
struct heap{int *a;
            int c;
            int maxc; };

struct heap*create(int n)
{struct heap*h=(struct heap*)malloc(sizeof(struct heap));
 h->maxc=n;
h->c=0;
h->a =(int*)malloc(n*sizeof(int));
return h;
}
int left(struct heap*h,int i)
{if((2*i +1)> (h->c-1)){return -1;}
	return (2*i+1);
}

int right(struct heap*h,int i)
{if((2*i +2)> (h->c-1)){return -1;}
	return (2*i +2);
}

int parent(struct heap*h,int i)
{if(i<=0 || i>=h->c){return -1;}
	return (i-1)/2;
}
void locate(struct heap*h,int i)
{
int l=left(h,i),r=right(h,i);
int max=i;
if(l!=-1 && h->a[l]>h->a[i])
	max=l;
if(r!=-1 && h->a[r]>h->a[max])
    max=r;
if(max!=i)
	{int t=h->a[i];
	  h->a[i]=h->a[max];
	  h->a[max]=t;
 locate(h,max);}

 }	  

void show(struct heap*h)
{int i=0;
for(;i<h->c;++i)
cout<<h->a[i]<<" ";}

void build(struct heap*h,int A[],int n)
{
 int i;
 for(i=0;i<n;++i)
{h->a[i]=A[i];}
h->c=n;
for(i=(n-1)/2;i>=0;i--)
{locate(h,i);}
}

int heapmax(struct heap*h)
{return (h->a[0]);}

void heapextract(struct heap*h)
{int t=h->a[0];
h->a[0]=h->a[h->c -1];
h->c--;
locate(h,0);}

void increasekey(struct heap*h,int i,int k)
{h->a[i]=k;
while(i>=0 && k>h->a[parent(h,i)])
	{h->a[i]=h->a[parent(h,i)];
    i=parent(h,i);
  }
  h->a[i]=k;

}

 void insert(struct heap*h,int k)
 {
 h->a[(h->c)++]=INT_MIN;
increasekey(h,h->c -1,k);}

void del(struct heap*h,int i)
{
h->a[i]=h->a[h->c -1];
h->c--;
locate(h,i);}

void sort(struct heap*h)
{int s=h->c;
	while(h->c>0)
{int t=h->a[0];
	h->a[0]=h->a[h->c-1];
h->a[h->c-1]=t;
h->c--;
locate(h,0);}
h->c=s;
}
int main()
{int n,A[10];
cout<<"enter size";cin>>n;
struct heap*h=create(n);
int i,k;cout<<"enter";
for(i=0;i<n;++i)
cin>>A[i];
build(h,A,n);
cout<<heapmax(h);
show(h);
heapextract(h);
cout<<"\n"<<heapmax(h);
show(h);
cout<<"enter i and k";cin>>i>>k;
increasekey(h,i,k);
show(h);
cout<<"enter insert value";
cin>>k;
insert(h,k);
show(h);
cout<<"enter i to delete";
cin>>i;
del(h,i);
sort(h);
show(h);}
